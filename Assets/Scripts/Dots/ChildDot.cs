using System;
using Strategies;
using UnityEngine;

namespace Dots
{
    public enum DotMode
    {
        Idle,
        Attack
    }
    public class ChildDot : Dot
    {
        public void AttachToMotherDot(MotherDot motherDot)
        {
            _motherDot = motherDot;
            motherDot.AddDot(this);
            team = _motherDot.team;
            AdjustColor();
        }

        public void Deattach()
        {
            _motherDot.RemoveDot(this);
        }
        
        public void AttackMotherDot(MotherDot motherDot)
        {
            _motherDot = motherDot;
            _mode = DotMode.Attack;
        }

        public int GetDamage()
        {
            return _strategy.GetDamage();
        }


        private DotMode _mode = DotMode.Idle;

        private float Radius => _strategy.GetRadius();
        private float RotationSpeed => _strategy.GetRotationSpeed();
        private float MoveSpeed => _strategy.GetMoveSpeed();
        
        private readonly Vector3 _rotationAxis = Vector3.forward;
        private MotherDot _motherDot;
        private BaseGameStrategy _strategy => BaseGameStrategy.Shared;

        protected override void Start()
        {
            base.Start();
            AdjustColor();
        }

        private void Update()
        {
            if (BaseGameStrategy.Shared.IsGameStopped)
                return;
            
            if (_mode == DotMode.Idle)
                RotateAroundMotherDot();
            else
                AttackMotherDot();
        }

        private void RotateAroundMotherDot()
        {
            if (_motherDot == null)
                return;
            
            var dotPosition = transform.position;
            var motherPosition = _motherDot.transform.position;
            
            transform.RotateAround (
                motherPosition, 
                _rotationAxis, 
                RotationSpeed * Time.deltaTime);
        }

        private void AttackMotherDot()
        {
            if (_motherDot == null)
                return;

            transform.position = Vector3.MoveTowards(
                transform.position,
                _motherDot.transform.position,
                MoveSpeed * Time.deltaTime
            );
        }

        private void OnTriggerEnter(Collider other)
        {
            var dot = other.GetComponent<ChildDot>();
            if (dot != null && dot.team != team)
            {
                Deattach();
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}