using System.Collections.Generic;
using Strategies;
using UnityEngine;
using UnityEngine.Serialization;

namespace Dots
{
    public class ChildDotFactory: MonoBehaviour
    {
        [FormerlySerializedAs("DotPrefab")] 
        public GameObject dotPrefab;
        
        public void GenerateChildDot(MotherDot motherDot)
        {
            if (BaseGameStrategy.Shared == null)
                return;
            
            if (!motherDot.HasFreeSpace)
                return;
            
            Vector3 motherPosition = motherDot.transform.position;
            var newDotObj = Instantiate(
                    dotPrefab, 
                    motherPosition + new Vector3(BaseGameStrategy.Shared.GetRadius(), 0),
                    Quaternion.identity
                );
            var dot = newDotObj.GetComponent<ChildDot>();
            dot.AttachToMotherDot(motherDot);
        }
    }
}
