using System;
using UnityEngine;
using UnityEngine.Serialization;
using Utils;

namespace Dots
{
    public abstract class Dot : MonoBehaviour
    {
        [FormerlySerializedAs("Team")] 
        public Team team;

        public virtual void SetOutline(bool visible)
        {
            transform.GetChild(0).gameObject.SetActive(visible);
        }
        
        
        
        protected Renderer Renderer;

        protected void AdjustColor()
        {
            if (team != null && Renderer != null)
            {
                Renderer.material = team.dotsMaterial;
            }
        }
        
        protected virtual void Start()
        {
            Renderer = GetComponent<Renderer>();
        }

    }
}