using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Dots
{
    /// <summary>
    /// Component for objects, which have health and can get damage
    /// </summary>
    public class Health : MonoBehaviour
    {
        [FormerlySerializedAs("MaxHealth")] 
        public int maxHealth = 100;

        public int CurrentHealth { get; private set; }

        public event Action<int> GotDamage;
        public event Action Death;

        public void Damage(int damage)
        {
            CurrentHealth -= Mathf.Min(damage, CurrentHealth);
            GotDamage?.Invoke(CurrentHealth);
            if (CurrentHealth == 0)
                Death?.Invoke();
        }

        private void Start()
        {
            CurrentHealth = maxHealth;
        }
    }
}