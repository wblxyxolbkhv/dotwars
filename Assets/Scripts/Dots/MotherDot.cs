using System;
using System.Collections.Generic;
using UnityEngine;

namespace Dots
{
    public class MotherDot : Dot
    {
        public override void SetOutline(bool visible)
        {
            base.SetOutline(visible);
            foreach (var dot in _dots)
            {
                dot.SetOutline(visible);
            }

            OutlineVisible = visible;
        }

        public void AttackDot(MotherDot motherDot)
        {
            foreach (var dot in _dots)
            {
                dot.AttackMotherDot(motherDot);
            }
            _dots.Clear();
        }

        public void AddDot(ChildDot dot)
        {
            _dots.Add(dot);
            dot.SetOutline(OutlineVisible);
        }

        public void RemoveDot(ChildDot dot)
        {
            _dots.Remove(dot);
        }
        
        
        public bool OutlineVisible { get; private set; } = false;
        public bool HasFreeSpace => _dots.Count < 6;


        
        
        
        private List<ChildDot> _dots = new List<ChildDot>();
        private Health _health;
        
        protected override void Start()
        {
            base.Start();
            AdjustColor();
            _health = GetComponent<Health>();
        }

        private void OnTriggerEnter(Collider other)
        {
            var dot = other.GetComponent<ChildDot>();
            if (dot != null && dot.team != team)
            {
                Destroy(other.gameObject);
                _health.Damage(dot.GetDamage());
            }
        }
    }
}