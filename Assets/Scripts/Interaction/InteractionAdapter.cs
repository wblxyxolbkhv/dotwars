using JetBrains.Annotations;
using Strategies;
using UnityEngine;

namespace Interaction
{
    /// <summary>
    /// Class, used for handle player interaction for several platforms
    /// </summary>
    public abstract class InteractionAdapter : MonoBehaviour
    {
        [CanBeNull]
        public GameObject GetTappedObject()
        {
            if (BaseGameStrategy.Shared.IsGameStopped)
                return null;
            
            var tapPosition = GetTapPosition();
            if (!tapPosition.HasValue)
                return null;
            
            var ray = Camera.main.ScreenPointToRay(tapPosition.Value);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100)) {
                return hit.transform.gameObject;
            }

            return null;
        }

        protected abstract Vector3? GetTapPosition();
    }
}