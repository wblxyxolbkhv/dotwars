using UnityEngine;

namespace Interaction
{
    public class MouseInteractionAdapter : InteractionAdapter
    {
        protected override Vector3? GetTapPosition()
        {
            if (Input.GetMouseButtonDown(0))
                return Input.mousePosition;
            return null;
        }
    }
}