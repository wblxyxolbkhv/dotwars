using UnityEngine;

namespace Interaction
{
    public class TouchInteractionAdapter : InteractionAdapter
    {
        protected override Vector3? GetTapPosition()
        {
            var touch = Input.GetTouch(0);
            if (touch.tapCount > 0)
                return touch.position;
            return null;
        }
    }
}