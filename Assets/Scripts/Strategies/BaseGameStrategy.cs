using System;
using UnityEngine;

namespace Strategies
{
    /// <summary>
    /// Game strategy - class which control several game characteristics
    /// and values. 
    /// </summary>
    public abstract class BaseGameStrategy: MonoBehaviour
    {
        public abstract int GetDotGenerationRate();
        public abstract float GetRadius();
        public abstract float GetRotationSpeed();
        public abstract float GetMoveSpeed();
        public abstract int GetDamage();


        public bool IsGameStopped { get; protected set; }

        public void GameOver()
        {
            IsGameStopped = true;
        }

        public static BaseGameStrategy Shared;
        
        private void Start()
        {
            if (Shared != null)
                Debug.LogError("More then 1 strategies on scene!");
            Shared = this;
        }
    }
}