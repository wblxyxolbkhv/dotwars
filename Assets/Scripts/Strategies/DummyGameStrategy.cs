namespace Strategies
{
    /// <summary>
    /// Strategy with hardcoded test values
    /// </summary>
    public class DummyGameStrategy: BaseGameStrategy
    {
        public override int GetDotGenerationRate()
        {
            return 3;
        }

        public override float GetRadius()
        {
            return 2;
        }

        public override float GetRotationSpeed()
        {
            return 100f;
        }

        public override float GetMoveSpeed()
        {
            return 10f;
        }

        public override int GetDamage()
        {
            return 10;
        }
    }
}