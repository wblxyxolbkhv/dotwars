using UnityEngine;

namespace Strategies
{
    /// <summary>
    /// Strategy for control from Unity Editor
    /// </summary>
    public class EditorGameStrategy : BaseGameStrategy
    {
        public int dotGenerationRate = 3;
        public float radius = 2f;
        public float rotationSpeed = 100f;
        public float moveSpeed = 10f;
        public int damage = 10;
        
        public override int GetDotGenerationRate()
        {
            return dotGenerationRate;
        }

        public override float GetRadius()
        {
            return radius;
        }

        public override float GetRotationSpeed()
        {
            return rotationSpeed;
        }

        public override float GetMoveSpeed()
        {
            return moveSpeed;
        }

        public override int GetDamage()
        {
            return damage;
        }
    }
}