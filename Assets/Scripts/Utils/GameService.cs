using System;
using System.Collections;
using Dots;
using Interaction;
using Strategies;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Utils
{
    /// <summary>
    /// The main util class, control game iteractions
    /// </summary>
    public class GameService : MonoBehaviour
    {
        [FormerlySerializedAs("RedMotherDot")] 
        public MotherDot redMotherDot;

        [FormerlySerializedAs("BlueMotherDot")] 
        public MotherDot blueMotherDot;

        
        [FormerlySerializedAs("RedTeamHpText")] 
        public Text redTeamHpText;
        
        [FormerlySerializedAs("BlueTeamHpText")] 
        public Text blueTeamHpText;

        
        [FormerlySerializedAs("GameOverPanel")] 
        public GameObject gameOverPanel;


        private BaseGameStrategy _strategy;
        private ChildDotFactory _dotFactory;
        private InteractionAdapter _interactionAdapter;


        private MotherDot _selectedDot;

        private void Start()
        {
            _strategy = GetComponent<BaseGameStrategy>();
            _dotFactory = GetComponent<ChildDotFactory>();
            _interactionAdapter = gameObject.AddComponent<MouseInteractionAdapter>();

            var blueHealth = blueMotherDot.GetComponent<Health>();
            blueHealth.GotDamage += BlueHealthOnGotDamage;
            blueHealth.Death += BlueHealthOnDeath;
            blueHealth.Damage(0);
            

            var redHealth = redMotherDot.GetComponent<Health>();
            redHealth.GotDamage += RedHealthOnGotDamage;
            redHealth.Death += RedHealthOnDeath;
            redHealth.Damage(0);
            
            StartCoroutine(GenerateDotsLoop());
        }

        #region Health event handlers

        
        private void RedHealthOnGotDamage(int currentHealth)
        {
            redTeamHpText.text = $"RED TEAM HP: {currentHealth}";
        }

        private void BlueHealthOnGotDamage(int currentHealth)
        {
            blueTeamHpText.text = $"BLUE TEAM HP: {currentHealth}";
        }
        
        private void RedHealthOnDeath()
        {
            GameOver();
        }
        
        private void BlueHealthOnDeath()
        {
            GameOver();
        }

        #endregion

        private IEnumerator GenerateDotsLoop()
        {
            if (!_strategy.IsGameStopped)
            {
                _dotFactory.GenerateChildDot(redMotherDot);
                _dotFactory.GenerateChildDot(blueMotherDot);
                yield return new WaitForSeconds(_strategy.GetDotGenerationRate());
                StartCoroutine(GenerateDotsLoop());
            }
        }

        private void Update()
        {
            var tappedObj = _interactionAdapter.GetTappedObject();
            if (tappedObj == null)
                return;

            var motherDot = tappedObj.GetComponent<MotherDot>();
            if (motherDot == null)
                return;

            if (_selectedDot == null)
            {
                SelectDot(motherDot);
            }
            else
            {
                if (_selectedDot == motherDot)
                {
                    DeselectDot();
                }
                else
                {
                    AttackDot(motherDot);
                }
            }
        }

        private void SelectDot(MotherDot motherDot)
        {
            _selectedDot = motherDot;
            motherDot.SetOutline(!motherDot.OutlineVisible);
        }

        private void DeselectDot()
        {
            if (_selectedDot == null)
                return;
            _selectedDot.SetOutline(false);
            _selectedDot = null;
        }
        
        private void AttackDot(MotherDot motherDot)
        {
            _selectedDot.AttackDot(motherDot);
        }

        private void GameOver()
        {
            _strategy.GameOver();
            gameOverPanel.SetActive(true);
            gameOverPanel.GetComponentInChildren<Button>().onClick.AddListener(() =>
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                });
        }
    }
}