using UnityEngine;
using UnityEngine.Serialization;

namespace Utils
{
    [CreateAssetMenu(fileName = "Team", menuName = "Team", order = 1000)]
    public class Team: ScriptableObject
    {
        [SerializeField]
        [FormerlySerializedAs("TeamName")] 
        public string teamName;
        
        [SerializeField]
        [FormerlySerializedAs("DotsMaterial")] 
        public Material dotsMaterial;
    }
}